/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"github.com/go-logr/logr"
	apiv1 "gitlab.com/wild-public/nginx-operator/api/v1"
	apps "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
)

// NginxAppReconciler reconciles a NginxApp object
type NginxAppReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	Log    logr.Logger
}

//+kubebuilder:rbac:groups=apps.wildapp.ml,resources=nginxapps,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=apps.wildapp.ml,resources=nginxapps/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=apps.wildapp.ml,resources=nginxapps/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the NginxApp object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.12.2/pkg/reconcile
func (r *NginxAppReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	//_ = r.Log.WithValues("Nginx operator", req.NamespacedName)
	log.Log.Info("Processing nginx app")
	nginx := &apiv1.NginxApp{}
	err := r.Client.Get(ctx, req.NamespacedName, nginx)

	if err != nil {
		if errors.IsNotFound(err) {
			log.Log.Info("Resource 'NginxApp' was not found")
			return ctrl.Result{}, nil
		}
		log.Log.Error(err, "Failed to get resource 'NginxApp'")
		return ctrl.Result{}, err
	}

	deployment := &apps.Deployment{}
	err = r.Client.Get(ctx, types.NamespacedName{Name: nginx.Name, Namespace: nginx.Namespace}, deployment)
	if err != nil && errors.IsNotFound(err) {
		app := r.deployNginxApp(nginx)
		log.Log.Info("Creating new deployment", "Deployment.Namespace", app.Namespace,
			"Deployment.Name", app.Name)
		err := r.Client.Create(ctx, app)
		if err != nil {
			log.Log.Error(err, "Failed to create deployment", "Deployment.Namespace", app.Namespace,
				"Deployment.Name", app.Name)
			return ctrl.Result{}, err
		}
		return ctrl.Result{Requeue: true}, nil
	} else if err != nil {
		log.Log.Error(err, "Failed to get deployment")
		return ctrl.Result{}, err
	}

	replicas := nginx.Spec.Replicas
	if *deployment.Spec.Replicas != replicas {
		deployment.Spec.Replicas = &replicas
		err := r.Client.Update(ctx, deployment)
		if err != nil {
			log.Log.Error(err, "Failed to scale replicas")
			return ctrl.Result{}, err
		}
		return ctrl.Result{Requeue: true}, nil
	}

	image := nginx.Spec.Image
	if deployment.Spec.Template.Spec.Containers[0].Image != image {
		deployment.Spec.Template.Spec.Containers[0].Image = image
		err := r.Client.Update(ctx, deployment)
		if err != nil {
			log.Log.Error(err, "Failed to change image")
			return ctrl.Result{}, err
		}
		return ctrl.Result{Requeue: true}, nil
	}

	imagePullPolicy := nginx.Spec.ImagePullPolicy
	if deployment.Spec.Template.Spec.Containers[0].ImagePullPolicy != imagePullPolicy {
		deployment.Spec.Template.Spec.Containers[0].ImagePullPolicy = imagePullPolicy
		err := r.Client.Update(ctx, deployment)
		if err != nil {
			log.Log.Error(err, "Failed to change imagePullPolicy")
			return ctrl.Result{}, err
		}
		return ctrl.Result{Requeue: true}, nil
	}
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *NginxAppReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&apiv1.NginxApp{}).
		Complete(r)
}

func (r *NginxAppReconciler) deployNginxApp(app *apiv1.NginxApp) *apps.Deployment {
	replicas := app.Spec.Replicas
	labels := app.Labels
	image := app.Spec.Image

	deployment := &apps.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      app.Name,
			Labels:    app.Labels,
			Namespace: app.Namespace,
		},
		Spec: apps.DeploymentSpec{
			Replicas: &replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{{
						Image:           image,
						Name:            app.Name,
						ImagePullPolicy: app.Spec.ImagePullPolicy,
					}},
				},
			},
		},
	}
	ctrl.SetControllerReference(app, deployment, r.Scheme)
	return deployment
}
